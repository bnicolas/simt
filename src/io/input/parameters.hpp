#ifndef SRC_IO_INPUT_PARAMETERS
#define SRC_IO_INPUT_PARAMETERS

#include <map>
#include <iostream>
#include <vector>
#include "ParEITConfig.hpp"

/* Manages options for simulations.
 *
 * Options are usually read from an input file, but can be set manually.
 * The input line format is the following:
 *
 * keyword = a list of options
 *
 * If the line ends with a backslash character (\),
 * options can be continued on the next line
 *
 */
class Parameters
{

public:
  Parameters();

  /* Constructor with a file name
   * fileName can be absolute or relative.
   */
  Parameters(std::string);

  /* Copy constructor
   * All options are copied from the configuration state,
   * not the input file (in case some entries were added/modified)
   */
  Parameters(const Parameters &cfg); // cfg=configuration.

  /* Assignment operator.
   * All options are copied from the configuration state,
   * not the input file (in case some entries were added/modified)
   */
  Parameters &operator=(Parameters &cfg);

  // Destructor. Nothing special to do.
  ~Parameters(){};

  // ===============================================================
  // Get values from config
  // ===============================================================

  // Return values associated to key. If key is missing from file,
  // program stops
  real_t getReal(std::string key);

  int_t getInt(std::string key);

  uint_t getUInt(std::string key);

  std::string getString(std::string key);

  // Returns value associated to key, if not in file, returns default value
  real_t getReal(std::string key, real_t defVal);

  int_t getInt(std::string key, int_t defVal);

  uint_t getUInt(std::string key, uint_t defVal);

  std::string getString(std::string key, std::string defVal);

  // ===============================================================
  // Modify configuration
  // ===============================================================

  // Replaces value in key, or creates entry if not existing
  void set(std::string key, std::string value);

  // ===============================================================
  // Quick information
  // ===============================================================

  // True if key is in the parameters map
  bool hasKey(std::string key);

  // ===============================================================
  // I/O
  // ===============================================================

  // Writes configuration in given file
  void save(std::string fileName);

  // Display configuration on standard output
  void display();

  friend std::ostream &operator<<(std::ostream &os, const Parameters &p);

private:
  /* Parses input file and store configuration.
   * clear: If true(default), previously defined options are deleted.
   *        When false, can be used to complete a configuration with another file.
   *        Previously existing entries are overwritten though.
   *
   *  The input line format is the following:
   *
   *  keyword = a list of options
   *
   *  If the line ends with a backslash character (\\),
   *  options can be continued on the next line
   *
   */
  void readInputFile(bool clear = true);

  // Prints an error message if file misses a key
  void errNoKey(std::string key) const;

  std::string m_fileName;                          // input file
  std::map<std::string, std::string> m_parameters; // configuration info
};

std::ostream &operator<<(std::ostream &os, const Parameters &p);

#endif /* SRC_IO_INPUT_PARAMETERS */
