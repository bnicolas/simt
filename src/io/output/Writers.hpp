#ifndef __GNUPLOT_HPP__
#define __GNUPLOT_HPP__

#include "geometry/Grid.hpp"

void
writeGnuplot (std::string filename, const Grid &grid, const EitBiVector<real_t> &vec, bool onlyItf);

#endif /* __GNUPLOT_HPP__ */
