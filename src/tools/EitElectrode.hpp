#ifndef SRC_TOOLS_EITELECTRODE
#define SRC_TOOLS_EITELECTRODE

#include "ParEITConfig.hpp"
#include <array>
#include <vector>

using ElectrodeThetas = std::array<real_t, 2>;

int_t getElectrodeIdForAngle(real_t theta, std::vector<ElectrodeThetas> &thetas);


//revoir ici
#endif /* SRC_TOOLS_EITELECTRODE */
