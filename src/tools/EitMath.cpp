#include "tools/EitMath.hpp"

real_t myAround(real_t x)
{
    real_t c = ceil(x);
    real_t f = floor(x);

    if (std::abs(x - c) < EPSILON)
        return c;
    else if (std::abs(x - f) < EPSILON)
        return f;
    return x;
}

real_t myDecimal(real_t x)
{
    real_t f = floor(x);
    return x - f;
}