#ifndef SRC_TOOLS_EITVECTOR
#define SRC_TOOLS_EITVECTOR

#include "ParEITConfig.hpp"
#include <vector>

class EitVector
{
public:
    using iterator = typename std::vector<real_t>::iterator;
    using const_iterator = typename std::vector<real_t>::const_iterator;
    using const_reverse_iterator = typename std::vector<real_t>::const_reverse_iterator;
    using reverse_iterator = typename std::vector<real_t>::reverse_iterator;

public:
    EitVector();

    EitVector(uint_t n);

    EitVector(const EitVector &that);

    ~EitVector();

    EitVector &operator=(const EitVector &that);

    uint_t size() const;

    void resize(uint_t n);

    real_t &operator[](uint_t index);

    const real_t &operator[](uint_t index) const;

    real_t &at(uint_t index);

    const real_t &at(uint_t index) const;

    void fill(real_t value);

    EitVector reciproqual() const;

    EitVector zero() const;

    real_t dotProduct(const EitVector &that) const;

    real_t norml2() const;

    iterator begin();

    const_reverse_iterator rbegin() const;

    const_iterator begin() const;

    const_iterator cbegin() const;

    const_reverse_iterator crbegin() const;

    iterator end();

    const_reverse_iterator rend() const; // j'ai modifie

    const_iterator end() const;

    const_iterator cend() const;

    const_reverse_iterator crend() const;

    real_t *data();

    const real_t *data() const;

    EitVector &operator+=(const EitVector &that);

    EitVector &operator+=(const real_t &coeff);

    EitVector &operator-=(const EitVector &that);

    EitVector &operator-=(const real_t &coeff);

    EitVector &operator*=(const EitVector &that);

    EitVector &operator*=(const real_t &coeff);

    EitVector &operator/=(const EitVector &that);

    EitVector &operator/=(const real_t &coeff);

protected:
    std::vector<real_t> *m_data = nullptr;
};

void checkSizes(std::string func, uint_t n, uint_t m);

EitVector operator+(const EitVector &a, const EitVector &b);

EitVector operator+(const EitVector &a, const real_t &coeff);

EitVector operator+(const real_t &coeff, const EitVector &a);

EitVector operator-(const EitVector &a, const EitVector &b);

EitVector operator-(const EitVector &a, const real_t &coeff);

EitVector operator-(const real_t &coeff, const EitVector &a);

EitVector operator*(const EitVector &a, const EitVector &b);

EitVector operator*(const EitVector &a, const real_t &coeff);

EitVector operator*(const real_t &coeff, const EitVector &a);

EitVector operator/(const EitVector &a, const EitVector &b);

EitVector operator/(const EitVector &a, const real_t &coeff);

EitVector operator/(const real_t &coeff, const EitVector &a);

#endif /* SRC_TOOLS_EITVECTOR */
