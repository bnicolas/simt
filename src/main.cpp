#include "geometry/Grid.hpp"
#include "io/input/parameters.hpp"
#include "io/output/Writers.hpp"
#include "solver/Matrix.hpp"
#include "solver/solver.hpp"

EIT_DECL_PARALLEL_DEFINITIONS;
/*
!!!!!! We are in the [-2 2] square !!!!!!
*/
int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &parallel::rank);
  MPI_Comm_size(MPI_COMM_WORLD, &parallel::size);
  std::cout << "rank : " << parallel::rank + 1 << " out of  " << parallel::size << std::endl;

  // Parameters params("params.in");
  GridDimension N = {10, 10};

  // Create the form :: a disk of radius 0.5 with 4 electrodes
  Form myform(1, 4);
  myform.coeff(0) = 0.5;
  // Setting the beginning angle of an electrode
  for (uint_t i = 0; i < 4; ++i)
  {
    myform.begAngle(i) = (i)*2. * EIT_PI / 4 - EIT_PI;
  }
  // Set the length of the elctrodes to 0.4 :: this will set the end angles via computation
  myform.buildThetas(0.4);
  // Print the beginning angle and the end angle of each electrode
  for (uint_t i = 0; i < 4; ++i)
  {
    std::cout << myform.getThetas()[i][0] << "  " << myform.getThetas()[i][1] << std::endl;
  }

  // Create the object grid of type Grid related to the object myform of type form.
  Grid grid(N, &myform);
  // Build the Grid Points
  grid.buildPoints();
  // Build the interface Points
  grid.buildInterfaces();
  // Build the halos for communications
  grid.buildHalos();
  // Modify neighbors
  grid.correctConnections();
  MPI_Barrier(MPI_COMM_WORLD);

  // print the points :: grid and interface.
  const std::vector<Point *> &pts = grid.getPoints();
  const std::vector<Point *> &itfs = grid.getInterfaces();
  const uint_t &n_pts = pts.size();
  const uint_t &n_itfs = itfs.size();
  // print the coordinates of everyone.
  for (int_t i = 0; i < (int_t)n_itfs; ++i)
  {
    std::cout << "[" << -(int_t)i - 1 << "] = "
              << "  " << itfs[i]->getCoordinates() << "  " << EIT_PROC_RANK << std::endl;
  }

  for (uint_t i = 0U; i < n_pts; ++i)
  {
    std::cout << "[" << i << "] = " << pts[i]->getCoordinates() << "  " << EIT_PROC_RANK << std::endl;
  }

  // creates an Eitbivector :: sigma
  EitBiVector<real_t> sigma = grid.createBiVector<real_t>();
  // object pf type Matrix :: A
  Matrix A(&grid, &myform, &sigma);

  EitBiVector<real_t> u = grid.createBiVector<real_t>();
  EitBiVector<real_t> v = grid.createBiVector<real_t>();

  for (uint_t i = 0U; i < grid.getNumberOfPoints(); ++i)
  {
    u.at(i) = i;
  }

  for (int_t i = 1; i <= (int_t)8 + EIT_ElEC; ++i)
  {
    u.at(-i) = -i;
  }

  for (uint_t i = 0U; i < grid.getNumberOfPoints(); ++i)
  {
    std::cout << u.at(i) << "  " << EIT_PROC_RANK << std::endl;
  }

  /* Undo the comment to print  the matrix vector product :: it will give you an mpi error for now, I need to modify the class Matrix, 
  I will puch the modication on gitlab as soon as possible */ 

  // std::cout<< "A*u gives the following EITbivector:"<<std::endl;

  //  v = A * u;
  // for (uint_t i = 0U; i < grid.getNumberOfPoints(); ++i)
  // {
  //   std::cout << v.at(i) << std::endl;
  // }

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();

  return 0;
}
