#ifndef __FORM_HPP__
#define __FORM_HPP__

#include "geometry/Point.hpp"
#include "tools/EitBiVector.hpp"
#include "tools/EitMath.hpp"
#include "tools/EitVector.hpp"

using FormCoeffs = std::vector<real_t>;
/* Class Form creates an object form that represents
the geometry in wich we resolve the EIT problem
*/
class Form
{
public:
  // Default constructor
  Form();
  // Constructor
  Form(uint_t n, uint ne);
  // Copy constructor
  Form(const Form &that);
  // Returns the ith component of the shape vector m_alpha(shape of the domain)
  real_t &
  coeff(uint_t i);
  // Returns the ith component of the shape vector m_thetas(positions of the electrodes)
  real_t &
  begAngle(uint_t i);
  //  Returns the shape vector m_thetas
  std::vector<ElectrodeThetas> &
  getThetas();
  // Returns the size of the shape vector alpha
  uint_t
  size() const;
  // Returns the radius of the form at each angle theta
  real_t
  radius(real_t theta);
  // Returns the first derivative of the radius
  real_t
  firstDerivativeofRadius(real_t theta);
  // Returns the segond derivative of the radius
  real_t
  secondDerivativeofRadius(real_t theta);
  // Returns  Rho=sqrt(radius*2 +radiusdarivative*2) at angle theta
  real_t
  rho(real_t theta);
  // Computes the end of the elctrode, knowing it's length "L" and it's begening angle "tm"
  real_t
  getEndAngleForLength(real_t L, uint_t i);
  // Returns thye value of the levelset function at point p
  real_t
  levelSet(const Point &pt);
  //  Parathesis operator that returnsthe value of the Levelset at Point pt
  real_t
  operator()(const Point &pt);
  // Returns the derivative of the levelset in a certain direction dir
  real_t
  derivativeLevelSet(const Point &pt, real_t dir, real_t step);
  // Sets the electrodes plaaced on the Form
  void
  buildThetas(real_t L);
  // Computes the interface point using a dichotomie method
  Point
  computeInterfacePoint(const Point &a, uint_t dim, real_t dir);
  // Computes the value of the delta function of Smereka.
  real_t
  approximateIntegral(const Point &pt, real_t &step);
  // Print operator ::  prints an object Form
  friend std::ostream &
  operator<<(std::ostream &os, const Form &f);

private:
  Point *m_center;// The center Point
  FormCoeffs m_alphas;// The values of the vector that determines shape are stored here
  std::vector<ElectrodeThetas> m_thetas;// The angles of the ectrodes are stored here
};
// print operator.
std::ostream &
operator<<(std::ostream &os, const Form &f);

#endif /* __FORM_HPP__ */
