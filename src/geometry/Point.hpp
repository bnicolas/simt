#ifndef SRC_GEOMETRY_POINT
#define SRC_GEOMETRY_POINT

#include <array>
#include <map>

#include "ParEITConfig.hpp"
#include "tools/EitArray.hpp"

typedef std::array<real_t, EIT_DIM> Vertex;
using GridDimension = std::array<uint_t, EIT_DIM>;
using PointNeighbors = std::array<int_t, 2 * EIT_DIM>;

int_t computeGlobalIndex(const GridDimension &N, const Vertex &ijk);
/* Class Point :: creation of an object point that represents
the point of a cartezian grid (x_i,y_j,z_k) up to the third dimension.
these points can be regular cartesian grid points. They also can be
 the intersection between the boundary and the geometrical form.
let's call them "interface points"*/
class Point
{
public:
  // Default constructor
  Point();
  // Constructor
  Point(Vertex ijk);
  // Copy constructor
  Point(const Point &that);
  // Equalizing operator
  Point &
  operator=(const Point &that);
  // Destructor
  ~Point();

  /* getting information on the object Point */
  // Returns the neighbors of an object Point
  const PointNeighbors &
  getNeighs() const;
  // Returns the coordinate array of an object Point
  const Vertex &
  getCoordinates() const;
  // Returns the i_th coordinate of an object Point
  const real_t &
  getCoordinate(uint i) const;
  // Retuns the component of the normal vector at the object Point
  const Vertex &
  getNormal() const;
  // Retuns the position (i,j,k) in cartesian grid of an object Point
  const Vertex &
  getIJK() const;
  /*Returns the normal stencil used in the discretization of the normal
    on the boundary. This is used for the interface Points*/
  const Vertex &
  getNormalStencil() const;
  // Is this an interface Point?
  bool
  isInterface() const;
  // Is my Point irregular?
  bool
  isIrregular() const;
  // Is my point placed on an electrode?
  const real_t &
  getisOnElectrode() const;

  /*Setting information for an object Point*/
  // Set the coordinates
  void
  setCoordinates(const Vertex &val);
  // Set the i_th coordinates
  void
  setCoordinate(uint_t i, const real_t &val);
  // Set the ith component of the normal vector
  void
  setNormal(uint_t i, const real_t &val);
  // Set the ith component of the normal stencil
  void
  setNormalStencil(uint_t i, const real_t &val);
  // Get the number of the electrode on wich we are placed
  void
  getElectrodeIdForAngle(std::vector<ElectrodeThetas> &thetas);
  // Set the value of (i,j,k)
  void
  setIJK(const Vertex &val);
  // Set the value of (i,j,k) individually (meaning component by component)
  void
  setIJK(uint_t i, const real_t &val);
  // Sets Point as the center
  void
  setCenter();
  // Returns the distance between the Point and the center Point
  real_t
  distance(const Point &center) const;
  // Returns the angle associated to the Point
  real_t
  angle() const;
  // Sets  m_isIrregular to false
  void
  setAsRegular(const GridDimension &N);
  // Sets  m_isIrregular to true
  void
  setAsIrregular(const PointNeighbors &neighs);
  // Sets m_isInterface to true
  void
  setAsInterface(const GridDimension &N);
  /* Operations on object Point */
  Point &
  operator+=(const Point &b);
  Point &
  operator-=(const Point &b);
  Point &
  operator*=(const real_t &k);
  // Print an object Point
  friend std::ostream &
  operator<<(std::ostream &os, const Point &p);

protected:
  Vertex m_ijk;// store (i,j,k)
  Vertex m_coor;// stores the coordinates of a Point
  PointNeighbors m_neighs; // stores the neighbors of a Point
  bool m_isInterface;// a bool that indicates if our Point is on the interface or not
  bool m_isIrregular;// a bool that indicates the type of a grid Point
  real_t m_isOnElectrode;// electrode number
  Vertex m_normal, m_norStencil;// stores normal values and normal stencils of a Point
};
  /* Operations on object Point with two entries */
Point operator+(const Point &a, const Point &b);

Point operator-(const Point &a, const Point &b);

Point operator*(const Point &a, const real_t &k);

Point operator*(const real_t &k, const Point &a);
// Print operator
std::ostream &
operator<<(std::ostream &os, const Point &p);

#endif /* SRC_GEOMETRY_POINT */
