#ifndef __GRID_HPP__
#define __GRID_HPP__

#include "geometry/Form.hpp"
#include "geometry/Point.hpp"
/* Class Grid creates an object grid that stores
all the information needed to create the linear system Ax=b*/
class Grid
{
public:
  // Default Constructor
  Grid(GridDimension N, Form *form);
  // Constructor
  Grid(const Grid &that);
  // Copy Constroctor
  Grid &
  operator=(const Grid &that);
  // Destructor
  ~Grid();
  /* Creates a vector of type bivector of type:: _Type
  size : n_neg =  the number of interface points + nb of electrodes(m_interfaces.size() + EIT_ElEC)
         n_pos = the number of grid points Nx*Ny (m_points.size()) */
  template <class _Type>
  EitBiVector<_Type> *
  createVector()
  {
    return new EitBiVector<_Type>(m_interfaces.size() + EIT_ElEC, m_points.size());
  }

  template <class _Type>
  EitBiVector<_Type>
  createBiVector()
  {
    return EitBiVector<_Type>(m_interfaces.size() + EIT_ElEC, m_points.size());
  }
  // Get the size of the cartesian grid
  const GridDimension &
  getSizes() const;
  // Get the pads of a cartesian grid
  const std::array<real_t, EIT_DIM> &
  getPads() const;
  // Get the local begining index on the proc
  uint_t
  getLocalBeg() const;
  // Get the local end index on the proc
  uint_t
  getLocalEnd() const;
  // Get the number of points on a proc (so it returns the size of m_points)
  uint_t
  getNumberOfPoints() const;
  // Get the number of interface Points (m_interface belongs to all procs)
  uint_t
  getNumberOfInterfaces() const;
  // Retuns a vector of type Point that stores the interface points
  const std::vector<Point *> &
  getInterfaces() const;
  //  Retuns the ith component of a vector of type Point that stores the grid points
  Point *
  getPoint(uint_t i);
  // Retuns a vector of type Point that stores the grid points locally
  const std::vector<Point *> &
  getPoints() const;
  // Get the value of the communicated objec of type Point from the right
  const std::vector<Point *> &
  getHaloR() const;
  // Get the value of the communicated objec of type Point from the left
  const std::vector<Point *> &
  getHaloL() const;

  std::vector<uint_t>
  getSizeAllProc();
  // Builds m_points locally
  void
  buildPoints();
  // Builds the left and right halo vectors for ecah proc
  void
  buildHalos();
  // Build m_intefaces
  void
  buildInterfaces();
  // Modifies the values of the neighbors of some objects Point so we can build later the correcvt linear system
  void
  correctConnections();

  void
  setForm(Form *form);

  void
  bcastSizes();

  Form *
  getForm() const;

protected:
  GridDimension m_N;
  uint_t m_NxBeg, m_NxEnd;
  Vertex m_h;
  std::vector<Point *> m_points;                  // points :: stores the grid Points
  std::vector<Point *> m_halo_left, m_halo_right; // points on other proc [me-1, me+1]
  std::vector<Point *> m_interfaces;              // interfaces points :: stores the interface Points
  Form *m_form;
  std::vector<uint_t> m_sizeproc;

  void
  toCoordinate(const Vertex &ijk, Vertex &coor);

  void
  toIJK(const Vertex &coor, Vertex &ijk);
};

#endif /* __GRID_HPP__ */
